class Vehicle {

    constructor(makeAndModel, rateCode) {
        this._makeAndModel = makeAndModel;
        this._rateCode     = rateCode;
    }

    getRateCode() {
        return this._rateCode;
    }

    getMakeAndModel() {
        return this._makeAndModel;
    }

    getTypeRules(){
        return types[this.getRateCode()].rules;
    }

    static get SUV() { return _SUV; }
    static get SEDAN() { return _SEDAN; }
    static get LUXURY() { return _LUXURY; }
    static get FOURxFOUR() { return _FOURxFOUR; }
}

const _SUV = 2;
const _SEDAN = 0;
const _FOURxFOUR = 1;
const _LUXURY = 3;

// @todo distribute this huge rule set among well-structured file hierarchy.
const types = {
    [_SUV]: {
        rules: {
            setupFees: 50,
            dailyRate: 150,
            dailyMileageLimit: 70,
            extraMileageRate: 2,
            discount: {
                amount: 5,
                type: 'percent',
                conditions: [
                    {parameter: "_kilometersRented", operator: '>=', value: 200}
                ]
            },
            latePenalty: {
                amount: 3,
                type: 'percent'
            }
        }
    },

    [_SEDAN]: {
        rules: {
            setupFees: 50,
            dailyRate: 100,
            dailyMileageLimit: 50,
            extraMileageRate: 2,
            discount: {
                amount: 0,
                type: 'percent',
                conditions: []
            },
            latePenalty: {
                amount: 3,
                type: 'percent'
            }
        }
    },

    [_FOURxFOUR]: {
        rules: {
            setupFees: 50,
            dailyRate: 200,
            dailyMileageLimit: 0,
            extraMileageRate: 0,
            discount: {
                amount: 5,
                type: 'percent',
                conditions: [
                    {parameter: "_daysRented", operator: '>', value: 10},
                    {parameter: "_kilometersRented", operator: '>=', value: 200}
                ]
            },
            latePenalty: {
                amount: 3,
                type: 'percent'
            }
        }
    },

    [_LUXURY]: {
        rules: {
            setupFees: 100,
            dailyRate: 250,
            dailyMileageLimit: 100,
            extraMileageRate: 3,
            discount: {
                amount: 50,
                type: 'flat',
                conditions: [
                    {parameter: "_kilometersRented", operator: '>=', value: 200}
                ]
            },
            latePenalty: {
                amount: 100,
                type: 'flat'
            }
        }
    }
}

module.exports = Vehicle

