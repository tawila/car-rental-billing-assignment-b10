const Statement = require("./Statement.js")

class Customer {

    constructor(name) {
        this._name    = name;
        this._rentals = [];
    }

    addRental(arg) {
        this._rentals.push(arg)
    }

    getName() {
        return this._name;
    }

    statement(format) {
        let statement = new Statement(this, this._rentals, format)
        return statement
            .calculate()
            .render();
    }
}

module.exports = Customer

