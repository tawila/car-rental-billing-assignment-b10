const Vehicle = require("./Vehicle.js")

class Rental {

    constructor(vehicle, mileage, daysRented, lateFee) {
        this._vehicle          = vehicle;
        this._kilometersRented = mileage;
        this._daysRented       = daysRented;
        this._lateFee          = lateFee;
    }

    getMileage() {
        return this._kilometersRented;
    }

    getVehicle() {
        return this._vehicle;
    }

    getDaysRented() {
        return this._daysRented;
    }

    isLate() {
        return this._lateFee;
    }

    vehicleRateCode() {
        return this.getVehicle().getRateCode();
    }

    vehicleMakeAndModel() {
        return this.getVehicle().getMakeAndModel();
    }

    getVehicleTypeRules(){
        return this.getVehicle().getTypeRules();
    }

    /**
     *
     * @returns {number}
     */
    calculateFees() {
        let thisAmount;
        let rentRules = this.getVehicleTypeRules();

        thisAmount = rentRules.setupFees;

        thisAmount += rentRules.dailyRate * this.getDaysRented();
        if (this.getMileage() > this.getDaysRented() * rentRules.dailyMileageLimit) {
            thisAmount += (this.getMileage() - this.getDaysRented() * rentRules.dailyMileageLimit) * rentRules.extraMileageRate;
        }

        thisAmount = this.applyDiscount(thisAmount, rentRules.discount);
        thisAmount = this.applyLateCharges(thisAmount, rentRules.latePenalty);

        return thisAmount;
    }

    applyDiscount(thisAmount, discount) {
        if (this.discountConditionsMet(discount.conditions)){
            switch (discount.type) {
                case 'percent':
                    thisAmount -= thisAmount * ( discount.amount / 100 );
                    break;
                case 'flat':
                    thisAmount -= discount.amount;
                    break;
            }
        }
        return thisAmount;
    }

    discountConditionsMet(conditions) {
        if (conditions.length === 0) {
            return true;
        }

        let passed = true;
        for (let i = 0; i < conditions.length; i++) {
            let conditionString = 'this.' + conditions[i].parameter + ' ' + conditions[i].operator + ' ' + conditions[i].value;
            passed = eval(conditionString);
            if (!passed)
                break;
        }

        return passed;
    }

    applyLateCharges(thisAmount, latePenalty) {
        if (this.isLate()) {
            switch (latePenalty.type) {
                case 'percent':
                    thisAmount += thisAmount * ( latePenalty.amount / 100 );
                    break;
                case 'flat':
                    thisAmount += latePenalty.amount;
                    break;
            }
        }
        return thisAmount;
    }

    /**
     *
     * @returns {number}
     * @todo Modify logic to ignore the previously existing points in calculations, after verifying with business ;-)
     */
    calculateRewardPoints(points) {
        // points = 0;     //this should fix the logic bug, uncomment after business logic approval.

        if (!this.isLate()) {
            // add frequent renter points
            points++;

            // add bonus for SUV rental
            if ((this.vehicleRateCode() === Vehicle.FOURxFOUR)) {
                points *= 2;
            }

            // add bonus for SUV rental
            if ((this.vehicleRateCode() === Vehicle.SUV) && this.getDaysRented() > 5) {
                points += (this.getDaysRented() - 5);
            }
        }

        return points;
    }
}

module.exports = Rental
