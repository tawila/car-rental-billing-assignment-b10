class StatementRendererJson {

    static render(statementData){
        let output = "";

        output += JSON.stringify(statementData);

        return output;
    }
}

module.exports = StatementRendererJson;