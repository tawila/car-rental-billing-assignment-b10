class StatementRendererPlainText {

    static render(statementData){
        let output = "";

        output += "Rental Record for:" + statementData.customerName + "\n";
        for (let i = 0; i < statementData.rentals.length; i++) {
            output += "\t\"" + statementData.rentals[i].vehicle + "\"\tLE " + statementData.rentals[i].amount.toFixed(2) + "\n";
        }
        output += "Amount owed is LE " + statementData.totalAmount.toFixed(2) + "\n";
        output += "You earned: " + statementData.rewardPoints + " new Reward Points\n\n";

        return output;
    }
}

module.exports = StatementRendererPlainText;