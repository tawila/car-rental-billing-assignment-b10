const StatementRendererPlainText = require("./StatementRendererPlainText.js")
const StatementRendererJson = require("./StatementRendererJson.js")

class Statement {

    constructor (customer, rentals, format)
    {
        this._customer = customer;
        this._rentals  = rentals;

        switch (format) {
            case Statement.JSON:
                this._renderer = StatementRendererJson;
                break;

            default:
            case Statement.PLAIN_TEXT:
                this._renderer = StatementRendererPlainText;
                break;
        }

        this.data = {};
    }

    calculate() {
        this.data.customerName = this._customer.getName();
        this.data.rentals      = [];
        this.data.totalAmount  = 0;
        this.data.rewardPoints = 0;

        for (let i = 0; i < this._rentals.length; i++) {
            this.data.rentals.push({
                vehicle: this._rentals[i].vehicleMakeAndModel(),
                amount:  this._rentals[i].calculateFees()
            });

            this.data.rewardPoints = this._rentals[i].calculateRewardPoints(this.data.rewardPoints);

            this.data.totalAmount += this.data.rentals[i].amount;
        }

        return this; // fluent interface
    }

    render() {
        let output = "";

        output += this._renderer.render(this.data);

        return output;
    }

    static get PLAIN_TEXT() { return _PLAIN; }
    static get JSON() { return _JSON; }
}

const _PLAIN = "text";
const _JSON  = "json";

module.exports = Statement;
