const assert = require('chai').assert
const Vehicle=require ("../src/Vehicle.js")
const Rental=require ("../src/Rental.js")
const Customer=require ("../src/Customer.js")
const Statement=require ("../src/Statement.js")

describe('Various Billing cases', () => {
  it('statement should return proper json when choosing JSON format', () => {

    let BlueBMW = new Vehicle("Red SangYoung 2017", Vehicle.SUV);
    let x3Rental = new Rental(BlueBMW, 270, 5, false);
    let sharmDreams = new Customer("Sharm Dreams");
    sharmDreams.addRental(x3Rental);

    assert.equal(
        sharmDreams.statement(Statement.JSON),
        '{"customerName":"Sharm Dreams","rentals":[{"vehicle":"Red SangYoung 2017","amount":760}],"totalAmount":760,"rewardPoints":1}'
    );

    assert.equal(
        sharmDreams.statement(Statement.PLAIN_TEXT),
        'Rental Record for:Sharm Dreams\n' +
        '\t"Red SangYoung 2017"\tLE 760.00\n' +
        'Amount owed is LE 760.00\n' +
        'You earned: 1 new Reward Points\n\n'
    )
  });

  it('SUVs should have bonus reward points for rentals above 5 days', () => {
    let RedX5BMW = new Vehicle("Red BMW x3 2017", Vehicle.SUV);
    let x3Rental = new Rental(RedX5BMW, 490, 8, false);
    let Ahmad = new Customer("Tawila.net");
    Ahmad.addRental(x3Rental);


    assert.equal(
        Ahmad.statement(Statement.PLAIN_TEXT),
        'Rental Record for:Tawila.net\n' +
        '\t"Red BMW x3 2017"\tLE 1187.50\n' +
        'Amount owed is LE 1187.50\n' +
        'You earned: 4 new Reward Points\n\n'
    )
  });

});
