const assert = require('chai').assert
const Vehicle=require ("../src/Vehicle.js")
const Rental=require ("../src/Rental.js")
const Customer=require ("../src/Customer.js")
const Statement=require ("../src/Statement.js")

describe('The vehicle Luxury type', () => {
    it('should calculate discount & penalty flat rate', () => {

        let Mercedes = new Vehicle("Mercedes C180 2017", Vehicle.LUXURY);
        let MyWeddingRental = new Rental(Mercedes, 270, 6, false);
        let SahelTripRental = new Rental(Mercedes, 680, 4, true);
        let Tawila = new Customer("Ahmad Tawila");

        Tawila.addRental(MyWeddingRental);
        Tawila.addRental(SahelTripRental);

        assert.equal(
            Tawila.statement(Statement.JSON),
            '{"customerName":"Ahmad Tawila","rentals":[{"vehicle":"Mercedes C180 2017","amount":1550},{"vehicle":"Mercedes C180 2017","amount":1990}],"totalAmount":3540,"rewardPoints":1}'
        );

        assert.equal(
            Tawila.statement(Statement.PLAIN_TEXT),
            'Rental Record for:Ahmad Tawila\n' +
            '\t"Mercedes C180 2017"\tLE 1550.00\n' +
            '\t"Mercedes C180 2017"\tLE 1990.00\n' +
            'Amount owed is LE 3540.00\n' +
            'You earned: 1 new Reward Points\n\n'
        )
    });
});
