# Car Rental Billing System

## usage
Please refer to PDF documentation emailed to the developer.

## Rules
    1- setup fees
        50 flat rate

    2- rate
        SEDAN:
            100LE per day
            50km per day
            2LE per extra km
        FOURxFOUR
            200LE per day
            0km per day
            0LE per extra km
        SUV
            150LE per day
            70km per day
            2LE per extra km



    3- Discounts
        SEDAN:
            nothing
        FOURxFOUR:
            discount 5%
            if (days > 10) & (km >= 200)
        SUV
            discount 5%
            if (km >= 200)


    4- Late charge
        additional 3% of the total amount.